<?php

/**
 * @file
 * Install, schema and uninstall functions for the module.
 */

/**
 * Implements hook_install().
 */
function gfpn4nodes_install() {
  \Drupal::messenger()->addStatus("Thank you for using this module, refer to the README.md for instructions!\nIf there is an issue with the module or you need a feature please use the issue queue:\nhttps://drupal.org/project/issues/gfpn4nodes");
}

/**
 * Implements hook_uninstall().
 */
function gfpn4nodes_uninstall() {
  \Drupal::messenger()->addStatus("Sad to see you go!\nIf there was an issue with the module or you need a feature please use the issue queue:\nhttps://drupal.org/project/issues/gfpn4nodes");
}

/**
 * Implements hook_schema().
 */
function gfpn4nodes_schema() {
  $schema['gfpn4nodes'] = [
    'description' => 'This table will hold information on all the device tokens which are subscribed to firebase notifications and which topics they are subscribed to.',
    'fields' => [
      'id' => [
        'type' => 'serial',
        'not null' => TRUE,
        'description' => 'Primary Key: Unique record ID.',
      ],
      'device_token' => [
        'description' => 'The device token that subscribed.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
      ],
      'topic' => [
        'description' => 'The topic(s) this device subscribed to.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
      ],
      'created_at' => [
        'description' => 'Timestamp when the record was created.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ],
    ],
    'primary key' => ['id'],
    'indexes' => [
      'device_token' => ['device_token'],
      'topic' => ['topic'],
    ],
  ];

  return $schema;
}
