/**
 * @file
 */
(function ($) {
    'use strict';
    $("td.views-field-operations li.send-fpn a").on("click", function (e) {
        var link = this;
        e.preventDefault();
        var check = confirm("Are you sure you want to send this push notification?");
        if (check) {
            window.open(link.href, '_blank');
        } else {
            console.log("Push notification cancelled.");
        }
    });
})(jQuery);
