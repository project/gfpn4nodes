<?php

namespace Drupal\gfpn4nodes\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\StreamWrapper\PublicStream;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\firebase\Service\FirebaseTopicManagerService;
use Drupal\file\FileInterface;

/**
 * Returns responses for Google Firebase Push Notifications for Nodes routes.
 */
class GoogleFpn4NodesController extends ControllerBase {

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Symfony\Component\HttpFoundation\RequestStack definition.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $request;

  /**
   * Drupal\Core\Database\Connection definition.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Drupal\firebase\Service\FirebaseMessageService definition.
   *
   * @var \Drupal\firebase\Service\FirebaseMessageService
   */
  protected $firebaseMessage;

  /**
   * Drupal\firebase\Service\FirebaseTopicManagerService definition.
   *
   * @var \Drupal\firebase\Service\FirebaseTopicManagerService
   */
  protected $firebaseTopicManager;

  /**
   * Drupal\Component\Datetime\TimeInterface definition.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->configFactory = $container->get('config.factory');
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->request = $container->get('request_stack');
    $instance->database = $container->get('database');
    $instance->firebaseMessage = $container->get('firebase.message');
    $instance->firebaseTopicManager = $container->get('firebase.topic_manager');
    $instance->time = $container->get('datetime.time');
    return $instance;
  }

  /**
   * Sends a Firebase Push Notification with Node data.
   */
  public function send(Request $request, $nid) {
    $config = $this->configFactory->get('gfpn4nodes.settings');
    $host = $this->request->getCurrentRequest()->getSchemeAndHttpHost();
    $publicPath = PublicStream::basePath();
    $node = $this->entityTypeManager->getStorage('node')->load($nid);
    $title = $node->title->value;
    $topicId = $node->get($config->get($node->bundle() . "_topic"))->target_id;

    // Change image field value if the field chosen from the form is of type entity reference (media)
    $absoluteImagePath = '';

    if ($node->get($config->get($node->bundle() . "_image"))->getFieldDefinition()->get("field_type") == "entity_reference") {
      $mediaId = $node->get($config->get($node->bundle() . "_image"))->first()->target_id;
      $media = $this->entityTypeManager->getStorage('media')->load($mediaId);
      $relativeImagePath = $media->field_image->entity->uri->value;
      $absoluteImagePath = str_replace("public:/", $host . "/" . $publicPath, $relativeImagePath);
    }
    else {
      $relativeImagePath = $node->get($config->get($node->bundle() . "_image"))->entity->uri->value;
      $absoluteImagePath = str_replace("public:/", $host . "/" . $publicPath, $relativeImagePath);
    }

    // Change body field value if the field chosen from the form is of type entity reference revisions (paragraphs)
    $body='';

    if ($node->get($config->get($node->bundle() . "_body"))->getFieldDefinition()->get("field_type") == "entity_reference_revisions") {
      $paragraphId = $node->get($config->get($node->bundle() . "_body"))->first()->target_id;
      $paragraph = $this->entityTypeManager->getStorage('paragraph')->load($paragraphId);
      $body = strip_tags($paragraph->field_text->value);
    }
    else {
      $body = strip_tags($node->get($config->get($node->bundle() . "_body"))->value);
    }

    $messageService = $this->firebaseMessage;
    if ($config->method_check == 'tokens') {
      if (count($config->device_tokens_file_id) > 0) {
        $csv_file_id = $device_tokens_file_id[0];
        $csv_file = $this->fileStorage->load($csv_file_id);
        if ($csv_file && $csv_file instanceof FileInterface) {
          $file_entity_uri = $csv_file->getFileUri();
          $file_pointer = fopen($file_entity_uri, "r");
          $csv_contents_array = [];
          if ($file_pointer) {
            while (($line = fgetcsv($file_pointer)) !== FALSE) {
              $csv_contents_array[] = $line;
            }
            fclose($file_pointer);
            $messageService->setRecipients($csv_contents_array);
          }
        }
      }
    }
    else {
      $messageService->setTopics((int) $topicId);
    }
    $messageService->setNotification([
      'title' => $title,
      'body' => $body,
      'badge' => 1,
      'image' => $absoluteImagePath,
      'sound' => 'default',
    ]);
    $messageService->setData([
      'nid' => $nid,
    ]);
    $messageService->setOptions(['priority' => 'high']);
    try {
      $messageService->send();
      return new JsonResponse("Notifications have been sent.");
    }
    catch (\Throwable $th) {
      throw $th;
    }
    return new JsonResponse("Sending of notifications has failed.");
  }

  /**
   * Subscribes a device token to a topic.
   */
  public function subscribe(Request $request) {
    $postData = json_decode($request->getContent());
    $connection = $this->database;
    $topicManager = $this->firebaseTopicManager;
    try {
      $topicManager->processTopicSubscription($postData->topic, [$postData->token], FirebaseTopicManagerService::SUBSCRIBE_ENDPOINT);
      $query = $connection->query("SELECT COUNT(*) FROM gfpn4nodes WHERE device_token='$postData->token' AND topic='$postData->topic'");
      $result = $query->fetchAll();
      $number_of_rows = 0;
      foreach ($result[0] as $key => $value) {
        if ($key == "COUNT(*)") {
          $number_of_rows = (int) $value;
        }
      }
      if ($number_of_rows > 0) {
        return new JsonResponse("Subscription of $postData->token to $postData->topic already exists.");
      }
      $result = $connection->insert('gfpn4nodes')
        ->fields([
          'device_token' => $postData->token,
          'topic' => $postData->topic,
          'created_at' => $this->time->getRequestTime(),
        ])
        ->execute();
      return new JsonResponse("Subscription of $postData->token to $postData->topic successful.");
    }
    catch (\Throwable $th) {
      throw $th;
    }
    return new JsonResponse("Subscription of $postData->token to $postData->topic unsuccessful.");
  }

  /**
   * Unsubscribes a device token from a topic.
   */
  public function unsubscribe(Request $request) {
    $postData = json_decode($request->getContent());
    $connection = $this->database;
    $topicManager = $this->firebaseTopicManager;
    try {
      $topicManager->processTopicSubscription($postData->topic, [$postData->token], FirebaseTopicManagerService::UNSUBSCRIBE_ENDPOINT);
      $connection->delete('gfpn4nodes')
        ->condition('device_token', $postData->token)
        ->condition('topic', $postData->topic)
        ->execute();
      return new JsonResponse("Unsubscription of $postData->token from $postData->topic successful.");
    }
    catch (\Throwable $th) {
      throw $th;
    }
    return new JsonResponse("Unsubscription of $postData->token from $postData->topic unsuccessful.");
  }

}
