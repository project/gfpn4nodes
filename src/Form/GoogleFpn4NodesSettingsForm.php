<?php

namespace Drupal\gfpn4nodes\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Google Firebase Push Notifications for Nodes settings for this site.
 */
class GoogleFpn4NodesSettingsForm extends ConfigFormBase {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\Core\Entity\EntityFieldManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityFieldManager
   */
  protected $entityFieldManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->entityFieldManager = $container->get('entity_field.manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'gfpn4nodes_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['gfpn4nodes.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    /**************** LOGIC ****************/
    $contentTypes = [];
    $contentTypesFields = [];
    $contentTypesTaxonomyTermReferenceFields = [];
    $contentTypesImageFields = [];
    $contentTypesTextFields = [];

    $contentTypesData = $this->entityTypeManager
      ->getStorage('node_type')
      ->loadMultiple();

    foreach ($contentTypesData as $value) {
      $contentTypes[$value->get("type")] = $value->get("type");
    }

    foreach ($contentTypesData as $bundle => $value) {
      if ($pos = strpos($value->get("entityTypeId"), "_") !== FALSE) {
        $entity_type = strtok($value->get("entityTypeId"), "_");
        $contentTypesFields[$bundle] = $this->entityFieldManager->getFieldDefinitions($entity_type, $bundle);
      }
    }

    foreach ($contentTypesFields as $bundle => $fields) {
      foreach ($fields as $field) {
        // Get all taxonomy term reference fields for topic mapping.
        if ($field->getType() == "entity_reference") {
          $referenceHandler = $field->getSetting("handler");
          if (str_contains($referenceHandler, ':taxonomy_term')) {
            $contentTypesTaxonomyTermReferenceFields[$bundle][$field->getName()] = $field->getName();
          }
          if (str_contains($referenceHandler, ':media')) {
            $contentTypesImageFields[$bundle][$field->getName()] = $field->getName();
          }
        }
        // Get all image reference fields for image mapping.
        if ($field->getType() == "image") {
          $contentTypesImageFields[$bundle][$field->getName()] = $field->getName();
        }
        
        // Get all text fields for body mapping.
        if ($field->getType() == "entity_reference_revisions") {
          $referenceHandler = $field->getSetting("handler");
          if (str_contains($referenceHandler, ':paragraph')) {
            $contentTypesTextFields[$bundle][$field->getName()] = $field->getName();
          }
        }
        if ($field->getType() == "text" || $field->getType() == "text_long" || $field->getType() == "text_with_summary" || $field->getType() == "string" || $field->getType() == "string_long") {
          if ($field->getName() != "title" && $field->getName() != "revision_log") {
            $contentTypesTextFields[$bundle][$field->getName()] = $field->getName();
          }
        }
      }
    }
    /**************** END LOGIC ****************/

    /**************** FORM BUILDING ****************/

    /**************** FIELDSETS ****************/
    $form['content_types_wrapper'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Content Types'),
      '#attributes' => ['id' => 'content_types_wrapper'],
    ];

    $form['targeting_method_wrapper'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Targeting Method'),
      '#attributes' => ['id' => 'targeting_method_wrapper'],
    ];

    $form['image_field_mapping_wrapper'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Map image fields'),
      '#attributes' => ['id' => 'image_field_mapping_wrapper'],
    ];

    $form['body_field_mapping_wrapper'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Map body fields'),
      '#attributes' => ['id' => 'body_field_mapping_wrapper'],
    ];

    $form['targeting_method_wrapper']['topic_field_mapping_wrapper'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Map topic fields'),
      '#attributes' => ['id' => 'topic_field_mapping_wrapper'],
      '#states' => [
        'visible' => [
          ':input[name="method_check"]' => ['value' => 'topics'],
        ],
      ],
    ];

    $form['targeting_method_wrapper']['device_tokens_file_wrapper'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Upload device token .csv'),
      '#attributes' => ['id' => 'device_tokens_file_wrapper'],
      '#states' => [
        'visible' => [
          ':input[name="method_check"]' => ['value' => 'tokens'],
        ],
      ],
    ];
    /**************** END FIELDSETS ****************/

    /**************** FIELDS ****************/
    $form['targeting_method_wrapper']['method_check'] = [
      '#type' => 'radios',
      '#title' => $this->t('Select which targeting method you wish to use for determining recipient devices.'),
      '#options' => [
        "tokens" => $this->t("Device Tokens"),
        "topics" => $this->t("Topic Subscriptions"),
      ],
      '#default_value' => $this->config('gfpn4nodes.settings')->get('method_check'),
      '#required' => TRUE,
      '#weight' => -1,
    ];

    $form['content_types_wrapper']['content_types'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Select which content types you want available for push notifications.'),
      '#options' => $contentTypes,
      '#default_value' => $this->config('gfpn4nodes.settings')->get('content_types'),
      '#multiple' => TRUE,
      '#required' => TRUE,
    ];

    $form['targeting_method_wrapper']['device_tokens_file_wrapper']['device_tokens_file_id'] = [
      '#title' => $this->t('Device Tokens .csv file'),
      '#attributes' => [
        'name' => 'device_tokens_file_id',
      ],
      '#type' => 'managed_file',
      '#description' => $this->t('Upload a .csv file with all the device tokens you wish to send push notifications to.'),
      '#upload_location' => 'public://' . date('Y-m'),
      '#default_value' => $this->config('gfpn4nodes.settings')->get('device_tokens_file_id'),
      '#upload_validators' => [
        'file_validate_extensions' => ['csv'],
      ],
    ];

    foreach ($contentTypesImageFields as $bundle => $imageReferenceFields) {
      $form['image_field_mapping_wrapper'][$bundle . "_image"] = [
        '#type' => 'select',
        '#description' => $this->t('Select the image reference field for the <strong>@bundle</strong> content type that defines your notification image.', ['@bundle' => ucfirst($bundle)]),
        '#title' => $this->t('@bundle image field', ['@bundle' => ucfirst($bundle)]),
        '#options' => $imageReferenceFields,
        '#default_value' => $this->config('gfpn4nodes.settings')->get($bundle . "_image"),
        '#validated' => TRUE,
        '#states' => [
          'visible' => [
            ':input[name="content_types[' . $bundle . ']"]' => ['checked' => TRUE],
          ],
        ],
      ];
    }

    foreach ($contentTypesTextFields as $bundle => $textFields) {
      $form['body_field_mapping_wrapper'][$bundle . "_body"] = [
        '#type' => 'select',
        '#description' => $this->t('Select the text field for the <strong>@bundle</strong> content type that defines your notification body.', ['@bundle' => ucfirst($bundle)]),
        '#title' => $this->t('@bundle body field', ['@bundle' => ucfirst($bundle)]),
        '#options' => $textFields,
        '#default_value' => $this->config('gfpn4nodes.settings')->get($bundle . "_body"),
        '#validated' => TRUE,
        '#states' => [
          'visible' => [
            ':input[name="content_types[' . $bundle . ']"]' => ['checked' => TRUE],
          ],
        ],
      ];
    }

    foreach ($contentTypesTaxonomyTermReferenceFields as $bundle => $termReferenceFields) {
      $form['targeting_method_wrapper']['topic_field_mapping_wrapper'][$bundle . "_topic"] = [
        '#type' => 'select',
        '#description' => $this->t('Select the taxonomy term reference field for the <strong>@bundle</strong> content type that defines your notification topics.', ['@bundle' => ucfirst($bundle)]),
        '#title' => $this->t('@bundle topic field', ['@bundle' => ucfirst($bundle)]),
        '#options' => $termReferenceFields,
        '#default_value' => $this->config('gfpn4nodes.settings')->get($bundle . "_topic"),
        '#states' => [
          'visible' => [
            ':input[name="content_types[' . $bundle . ']"]' => ['checked' => TRUE],
          ],
        ],
      ];
    }
    /**************** END FIELDS ****************/

    $form['#attached']['library'][] = 'gfpn4nodes/styling';
    /**************** END FORM BUILDING ****************/

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('gfpn4nodes.settings')
      ->set('content_types', $form_state->getValue('content_types'))
      ->set('method_check', $form_state->getValue('method_check'))
      ->set('image_check', $form_state->getValue('image_check'))
      ->set('device_tokens_file_id', $form_state->getValue('device_tokens_file_id'))
      ->save();

    foreach ($form['targeting_method_wrapper']['topic_field_mapping_wrapper'] as $key => $value) {
      if (gettype($value) == "array") {
        if (array_key_exists('#input', $value) && $value["#input"] == TRUE) {
          if (!empty($form_state->getValue($key))) {
            $this->config('gfpn4nodes.settings')
              ->set($key, $form_state->getValue($key))
              ->save();
          }
        }
      }
    }

    foreach ($form['image_field_mapping_wrapper'] as $key => $value) {
      if (gettype($value) == "array") {
        if (array_key_exists('#input', $value) && $value["#input"] == TRUE) {
          if (!empty($form_state->getValue($key))) {
            $this->config('gfpn4nodes.settings')
              ->set($key, $form_state->getValue($key))
              ->save();
          }
        }
      }
    }

    foreach ($form['body_field_mapping_wrapper'] as $key => $value) {
      if (gettype($value) == "array") {
        if (array_key_exists('#input', $value) && $value["#input"] == TRUE) {
          if (!empty($form_state->getValue($key))) {
            $this->config('gfpn4nodes.settings')
              ->set($key, $form_state->getValue($key))
              ->save();
          }
        }
      }
    }

    foreach ($form['image_file_wrapper'] as $key => $value) {
      if (gettype($value) == "array") {
        if (array_key_exists('#input', $value) && $value["#input"] == TRUE) {
          if (!empty($form_state->getValue($key))) {
            $this->config('gfpn4nodes.settings')
              ->set($key, $form_state->getValue($key))
              ->save();
          }
        }
      }
    }

    parent::submitForm($form, $form_state);
  }

}
